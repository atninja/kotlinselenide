package test

import com.codeborne.selenide.WebDriverRunner
import com.codeborne.selenide.logevents.SimpleReport
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver

class GoogleSearchTest : Page() {

    lateinit var driver: WebDriver
        private set

    private val sr = SimpleReport()

    @Before
    fun setUp() {
        System.setProperty("webdriver.chrome.driver", "src/test/kotlin/test/resources/chromedriver.exe")
        driver = ChromeDriver()
        WebDriverRunner.setWebDriver(driver)
    }

    @Test
    fun test() {
        sr.start()
        GoogleSearchPage()
                .openPage()
                .setValueInSearchField("some value")
                .clickOnSearchBtn()
        sr.finish("googleTest")
    }

    @After
    fun tearDown() {
        driver.close()
    }
}
