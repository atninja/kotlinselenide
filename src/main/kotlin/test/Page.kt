package test

import com.codeborne.selenide.ElementsCollection
import com.codeborne.selenide.Selenide
import com.codeborne.selenide.SelenideElement
import org.openqa.selenium.By

open class Page {
    fun s(css: String): SelenideElement {
        return Selenide.`$`(css)
    }

    fun ss(css: String): ElementsCollection {
        return Selenide.`$$`(css)
    }

    fun sx(xpath: String): SelenideElement {
        return Selenide.`$`(By.xpath(xpath))
    }

    fun ssx(xpath: String): ElementsCollection {
        return Selenide.`$$`(By.xpath(xpath))
    }
}