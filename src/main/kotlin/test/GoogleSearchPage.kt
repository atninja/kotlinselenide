package test

import com.codeborne.selenide.Selenide.open

class GoogleSearchPage : Page() {

    private val pageUrl = "https://www.google.com/"
    private val searchField = s(".gsfi")
    private val searchButton = s("[value=\"Поиск в Google\"]")

    fun openPage(): GoogleSearchPage {
        open(pageUrl)
        return this
    }

    fun setValueInSearchField(valueToSearch: String): GoogleSearchPage {
        searchField.value = valueToSearch
        return this
    }

    fun clickOnSearchBtn(): GoogleSearchPage {
        searchButton.click()
        return this
    }
}